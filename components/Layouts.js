import Head from "next/head"

const Layouts = (props) => {
    const { children, title } = props;
    const titlePg = process.env.appName + (title && ` | ${title}` )
    return (
        <div>
            <Head>
                <title>{titlePg}</title>
            </Head>

            <div className="py-5">
                {children}
            </div>
        </div>
    )
}

export default Layouts