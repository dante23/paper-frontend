import Layouts from "../components/Layouts";
const Home = () => {
  return (
    <div>
      <Layouts title="Home">
          <div className="max-w-screen-md border rounded-lg shadow-md p-6 ml-40 flex">
            Home
          </div>
      </Layouts>
    </div>
  )
}

export default Home
